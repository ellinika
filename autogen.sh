#! /bin/sh

git submodule init
git submodule update

cat > ChangeLog <<EOT
This file is a placeholder. It will be replaced with the actual ChangeLog
by make.
EOT

if [ ! -d html ]; then
	mkdir html
fi
if [ ! -d tmp ]; then
	mkdir tmp
fi
autoreconf -f -i -s
