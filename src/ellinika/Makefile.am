# This file is part of Ellinika project.
# Copyright (C) 2004, 2006-2008, 2011, 2015 Sergey Poznyakoff
#
# Ellinika is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# Ellinika is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

sitedir=$(GUILE_SITE)/$(PACKAGE)
site_DATA=\
 xlat.scm\
 cgi.scm\
 i18n.scm\
 config.scm\
 dico.scm\
 elmorph.scm\
 tenses.scm\
 sql.scm\
 conjugator.scm

cgi.m4: Makefile
	$(AM_V_GEN){							\
		echo 'divert(-1)';					\
		echo 'changequote([,])';				\
		echo 'changecom([;],[';					\
		echo '])';						\
		echo 'undefine([format])';				\
		echo 'define([IFACE],[$(APACHE_IFACE)])';		\
		echo 'define([SCRIPT_SUFFIX], [@SCRIPT_SUFFIX@])';	\
		echo 'define([GUILE_BINDIR],[$(GUILE_BINDIR)])';	\
		echo 'define([GUILE_SITE],[@GUILE_SITE@])';		\
		echo 'define([PACKAGE],[$(PACKAGE)])';			\
		echo 'define([PREFIX],[$(prefix)])';			\
		echo 'define([SYSCONFDIR],[$(sysconfdir)])';		\
		echo 'define([LOCALEDIR],[$(datadir)/locale])';		\
		echo 'define([HTMLDIR],[$(HTMLDIR)])';			\
		echo 'define([VERSION],[$(VERSION)])';			\
		echo 'define([LIBDIR],[$(pkglibdir)])';			\
		echo 'divert(0)dnl';					\
		echo '@AUTOGENERATED@';					\
	} > cgi.m4

SUFFIXES = .scm4 .scm .x

.scm4.scm:
	$(AM_V_GEN)m4 -I$(srcdir) cgi.m4 $< > $@ 

cgi.scm: cgi.scm4 cgi.m4
config.scm: config.scm4 cgi.m4
elmorph.scm: elmorph.scm4 elmorph-public.scm cgi.m4

pkglib_LTLIBRARIES=libelmorph.la

libelmorph_la_SOURCES = \
 aorist.c\
 utf8.c\
 utf8scm.c\
 elchr.c\
 elmorph.c\
 elmorph.h\
 phoneme.y\
 syllabificator.c

DOT_X_FILES = elmorph.x utf8scm.x
BUILT_SOURCES = $(DOT_X_FILES)
DISTCLEANFILES = $(DOT_X_FILES)

phoneme.h phoneme.c: phoneme.y
elchr.c: phoneme.h

pkglibnames=elmorph

install-data-hook:
	here=`pwd`; \
	 cd $(DESTDIR)$(pkglibdir);\
	 for name in $(pkglibnames); do \
	   if test -f lib$$name.la; then \
	     dlname=`sed -n 's/dlname='\''\(.*\)'\''/\1/p' lib$$name.la`; \
	     test -z "$$dlname" && dlname='lib$$name.so'; \
	     $(LN_S) -f "$$dlname" libguile-$$name-v-$(VERSION).so; \
	   fi; \
	 done; \
	 cd $$here

AM_YFLAGS = -d
YACCCOMPILE = $(srcdir)/yyrename '$(YACC) $(YFLAGS) $(AM_YFLAGS)'
EXTRA_DIST = yyrename elmorph-public.scm

AM_CPPFLAGS=-I.
CLEANFILES=
MAINTAINERCLEANFILES=
include ../../gint/gint.mk

