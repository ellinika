(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")

(display
 (elstr-suffix? (string->elstr "παρακείμενος") "μενος"))
(newline)
(display
 (elstr-suffix? (string->elstr "παρακείμενος") "μεν"))
(newline)
(display
 (elstr-suffix? (string->elstr "παρακείμενος") "α" "οντας" "μενος"))
(newline)
