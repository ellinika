(use-modules (ellinika xlat))

(let ((str "mila'v"))
  (display (ellinika:translate-kbd str))
  (newline))
