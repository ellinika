(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")
(load "../elmorph-public.scm")

(let ((word "παρακείμενος"))
  (display (elstr-trim word -1))
  (newline)
  (elstr-trim! word -1)
  (display word)
  (newline))
