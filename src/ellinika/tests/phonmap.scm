(load-extension "./libelmorph" "scm_init_ellinika_elmorph_module")

(let ((word (string->elstr "παρακείμενος")))
  (display (elstr->phonetic-map word))
  (newline))
