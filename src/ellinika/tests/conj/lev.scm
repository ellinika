(use-modules ((ellinika test-conjugation)))

(test-conjugation:verb "λέω")
;(test-conjugation:tense "λέω" "pas" "ind" "Αόριστος")
;(test-conjugation:tense "λέω" "act" "ind" "Ενεστώτας")
;(test-conjugation:tense "λέω" "act" "ind" "Παρακείμενος")
;(test-conjugation:tense "λέω" "act" "ind" "Μέλλοντας διαρκείας")
;(test-conjugation:tense "λέω" "act" "ind" "Παρατατικός")
;(test-conjugation:tense "λέω" "pas" "ind" "Μέλλοντας στιγμιαίος")
;(test-conjugation:tense "λέω" "pas" "imp" "Αόριστος")