/* This file is part of Ellinika project.
   Copyright (C) 2011, 2024 Sergey Poznyakoff

   Ellinika is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   Ellinika is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <libguile.h>
#include "utf8.h"
#include "elmorph.h"

int
elmorph_thema_aoristoy(unsigned *word, size_t len,
		       unsigned **thema, size_t *tlen)
{
	unsigned ch, *pw;
	
	switch (word[len-1]) {
	case 0x03B6: /* ζ */
		/* FIXME: This can produce ξ as well: αλλάζω => άλλαξα */
	case 0x03B8: /* θ */
		ch = 0x03C3; /* σ */
		break;

	case 0x03B3: /* γ */
	case 0x03C7: /* χ */
		ch = 0x03BE; /* ξ */
		break;
		
	case 0x03BA: /* κ */
		if (len > 1 && word[len-2] == 0x03C3 /* σκ */)
			len--;
		ch = 0x03BE; /* ξ */
		break;

	case 0x03BD: /* ν */
		if (len > 1 && word[len-2] == 0x03C7 /* χν */) {
			len--;
			ch = 0x03BE; /* ξ */
		} else
			ch = 0x03C3; /* σ */
		break;

	case 0x03B2: /* β */
	case 0x03C0: /* π */
	case 0x03C6: /* φ */
		ch = 0x03C8; /* ψ */
		break;

	case 0x03CD: /* ύ */
	case 0x03C5: /* υ FIXME: This assumes the word has been deaccentized */
		if (len > 1 && (word[len-2] == 0x03B1 /* αύ */ ||
				word[len-2] == 0x03B5 /* εύ */)) {
			ch = 0x03C8; /* ψ */
			break;
		}

	default:
		len++;
		ch = 0x03C3; /* σ */
	}

	pw = calloc(len, sizeof(pw[0]));
	if (!pw)
		return -1;
	memcpy(pw, word, sizeof(word[0]) * (len - 1));
	pw[len-1] = ch;

	*thema = pw;
	*tlen = len;
	return 0;
}

		
		
