;;;; This file is part of Ellinika project.
;;;; Copyright (C) 2011 Sergey Poznyakoff
;;;;
;;;; Ellinika is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; Ellinika is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(use-modules ((srfi srfi-1)))

(define-public (elstr-trim word n)
  (let ((word (if (string? word)
		  (string->elstr word)
		  word)))
    (cond
     ((> n 0)
      (elstr-slice word n (- (elstr-length word) n)))
     ((< n 0)
      (elstr-slice word 0 (+ (elstr-length word) n)))
     (else
      word))))

(define-public (elstr-trim! word n)
  (let ((word (if (string? word)
		  (string->elstr word)
		  word)))
    (cond
     ((> n 0)
      (elstr-slice! word n (- (elstr-length word) n)))
     ((< n 0)
      (elstr-slice! word 0 (+ (elstr-length word) n))))))

(define-public (phoneme:code ph)
  (list-ref ph 0))

(define-public (phoneme:start ph)
  (list-ref ph 1))

(define-public (phoneme:count ph)
  (list-ref ph 2))

(define-public (phoneme:flags ph)
  (list-ref ph 3))

(define-public (phoneme:accented? ph)
  (logand (phoneme:flags ph) elmorph:accent-mask))

(define-public (phoneme:vowel? ph)
  (= (logand (phoneme:flags ph) elmorph:vowel)))

(define-public (phoneme:consonant? ph)
  (= (logand (phoneme:flags ph) elmorph:consonant)))

(define-public (phoneme:diphthong? ph)
  (= (logand (phoneme:flags ph) elmorph:diphthong)))


(define soundslike-transcription-list
  '((1 . "a")
    (2 . "e")
    (3 . "i")
    (4 . "o")
    (5 . "u")
    (6 . "b")
    (7 . "g")
    (8 . "d")
    (9 . "z")
    (10 . "t")
    (11 . "k")
    (12 . "l")
    (13 . "m")
    (14 . "n")
    (15 . "x")
    (16 . "p")
    (17 . "r")
    (18 . "s")
    (19 . "t")
    (20 . "f")
    (21 . "h")
    (22 . "P")
    (23 . "b")
    (24 . "d")
    (25 . "g")
    (26 . "sm")
    (27 . "ts")
    (28 . "tz")
    (29 . "ngz")
    (30 . "au")
    (31 . "eu")))

(define-public (elstr->soundslike word)
  (let ((phon-map (elstr->phonetic-map word)))
    (apply string-append
	   (filter-map
	    (lambda (elt)
	      (assoc-ref soundslike-transcription-list (phoneme:code elt)))
	    phon-map))))

