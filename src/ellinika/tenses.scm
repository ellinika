;;;; This file is part of Ellinika
;;;; Copyright (C) 2011 Sergey Poznyakoff
;;;;
;;;; Ellinika is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 3 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; Ellinika is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program.  If not, see <http://www.gnu.org/licenses/>.
;;;;
(define-module (ellinika tenses))

(define-public ellinika-tense-list
  (list
   (cons "ind"
	 (list "Ενεστώτας"
	       "Παρατατικός"
	       "Μέλλοντας διαρκείας"
	       "Αόριστος"
	       "Παρακείμενος"
	       "Υπερσυντέλικος"
	       "Συντελεσμένος μέλλοντας"
	       "Μέλλοντας στιγμιαίος"))
   (cons "sub"
	 (list "Ενεστώτας"
	       "Αόριστος"
	       "Παρακείμενος"))
   (cons "imp"
	 (list "Ενεστώτας"
	       "Αόριστος"
	       "Παρακείμενος"))))

(define-public ellinika-conjugation-term-transtab
  '(("act" . "Ενεργητηκή φωνή")
    ("pas" . "Μεσοπαθητική φωνή")
    ("ind" . "Οριστική")
    ("sub" . "Υποτακτική")
    ("imp" . "Προστακτική")))

(define-public (ellinika-conjugation-term x)
  (or (assoc-ref ellinika-conjugation-term-transtab x) x))


