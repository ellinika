<!--

  Copyright (C) 2006 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<PAGE PREFIX="arthra" HEADER="Przedimek">

<PARA><DFN>Przedimek</DFN> (nazywany również <DFN>rodzajnikiem</DFN>)
--- służbowa część mowy, która określa kategorię określoności lub
nieokreśloności następującego za nim rzeczownika.
</PARA>

<PARA>Język grecki posiada przedimki określone (οριστικό άρθρο) i
nieokreślone (αόριστο άρθρο).</PARA>

<SECTION>
 <HEADER>Przedimek określony</HEADER>

<TABULAR ALTERNATE="1" ROWHEADING="std" COLHEADING="std">
 <TITLE>Odmiana przedimka określonego</TITLE>
 <ROW>
  <ITEM />
  <ITEM>αρσενικό</ITEM>
  <ITEM>θυλικό</ITEM>
  <ITEM>ουδέτερο</ITEM>
 </ROW>  
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>ο</ITEM>
  <ITEM>η</ITEM>
  <ITEM>το</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>του</ITEM>
  <ITEM>της</ITEM>
  <ITEM>του</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τον</ITEM>
  <ITEM>την</ITEM>
  <ITEM>το</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ονομαστική</ITEM> 	
  <ITEM>οι</ITEM>
  <ITEM>οι</ITEM>
  <ITEM>τα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM> 	
  <ITEM>των</ITEM>
  <ITEM>των</ITEM>
  <ITEM>των</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τους</ITEM>
  <ITEM>τις</ITEM>
  <ITEM>τα</ITEM>
 </ROW>
</TABULAR>

<ANCHOR ID="oristiko_arthro" />
<PARA>Rodzajnika określonego używamy:</PARA>

<ENUMERATE>
<ITEM>zwykle przed nazwami własnymi, nazwami narodów oraz nazwami
geograficznymi: <EXAMPLE>ο Γιάννης</EXAMPLE> <TRANS>Janis</TRANS>,
<EXAMPLE>ο Έλληνας</EXAMPLE> <TRANS>Grek</TRANS>, <EXAMPLE>οι
Αθήναι</EXAMPLE> <TRANS>Ateny</TRANS></ITEM>

<ITEM>zawsze po zaimku wskazującym, który występuje w roli określenia:
<EXAMPLE>αυτό το βιβλίο</EXAMPLE> <TRANS>ta księga</TRANS>. Jeśli
rzeczownik jest określony przez zaimek wskazujący i przymiotnik,
przedimek stawiamy przed przymiotnikiem: <EXAMPLE>αυτή η ωραία
γυναίκα</EXAMPLE> <TRANS>ta piękna kobieta</TRANS>.</ITEM>

<ITEM>po przymiotnikach zaimkowych <DICTREF>όλος</DICTREF>,
<DICTREF>ολόκληρος</DICTREF>, <DICTREF>ολάκερος</DICTREF> oraz po
zaimku względnym <DICTREF>οποίος</DICTREF>: <EXAMPLE>όλοι οι
άνθρωποι</EXAMPLE> <TRANS>wszyscy ludzie</TRANS>, <EXAMPLE>τον οποίον
είδα</EXAMPLE> <TRANS>ten, którego widziałem</TRANS>.</ITEM>

<ITEM>przed rzeczownikami, określonymi przez enklityczny zaimek
dzierżawczy: <EXAMPLE>το βιβλίο μου</EXAMPLE> <TRANS>moja
książka</TRANS>, <EXAMPLE>το δικό μου βιβλίο</EXAMPLE> <TRANS>moja
(własna) książka</TRANS>.</ITEM>

<ITEM>przed substantywizacja częścią mowy: <EXAMPLE>ο
<DICTREF>ευτυχής</DICTREF></EXAMPLE> <TRANS>szczęściarz</TRANS></ITEM>

</ENUMERATE>

<PARA>Zwykłym szykiem słów jest: przedimek - określenie - rzeczownik,
na przykład: <EXAMPLE>το ψηλό δέντρο</EXAMPLE> <TRANS>wysokie
drzewo</TRANS>. Niemniej jednak, określenie może występować również po
rzeczowniku. W takim przypadku przedimek występuje dwukrotnie:
<EXAMPLE>το δέντρο το ψηλό</EXAMPLE>.</PARA> 

<PARA>W wypadku gdy przedimek określa kilka złączonych
kontekstem rzeczowników, często używa się wspólnego przedimka:
<EXAMPLE>τα ξάρτια και πανιά</EXAMPLE> <TRANS>liny i żagli</TRANS>.</PARA>    

<PARA>Przedimka nie używamy:</PARA>

<ENUMERATE>
<ITEM>Przed orzecznikiem rzeczownikowym: <EXAMPLE>είμαι γιατρός</EXAMPLE>
<TRANS>jestem lekarzem</TRANS>.</ITEM>

<ITEM>W tytułach, szyldach, obwieszczeniach i tp.</ITEM>

<ITEM>W spisach: <EXAMPLE>σε κείνα τα μέρη πού βουνά και κάμποι
είναι ντυμένοι λευκή στολή</EXAMPLE> <TRANS>w tych krajach, gdzie
góry i równiny ubrane są w białą odzież</TRANS>.</ITEM>

<ITEM>W niektórych zwrotach idiomatycznych: <EXAMPLE>σηκώνει
κεφάλι</EXAMPLE> <TRANS>podnosi głowę</TRANS>, <EXAMPLE>έχει καλό
αυτί</EXAMPLE> <TRANS>ma dobry słuch</TRANS>.</ITEM> 

</ENUMERATE>

<PARA>Żeby zapobiec powtórzeniu tego samego rzeczownika w jednym
zdaniu, używamy przedimka, zamiast podalszych występowań rzeczownika:
<EXAMPLE>η κατοικία μας και η των γειτόνων μας</EXAMPLE> <TRANS>
nasze mieszkanie i mieszkanie naszych sąsiadów</TRANS>.</PARA> 

</SECTION>

<SECTION ID="aoristo_arthro">
<HEADER>Przedimek nieokreślony</HEADER>

<TABULAR ALTERNATE="1" ROWHEADING="std" COLHEADING="std">
 <TITLE>Odmiana przedimka nieokreślonego</TITLE>
 <ROW>
  <ITEM />
  <ITEM>αρσενικό</ITEM>
  <ITEM>θυλικό</ITEM>
  <ITEM>ουδέτερο</ITEM>
 </ROW>
 
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>ένας</ITEM>
  <ITEM>μια</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>

 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>ενός</ITEM>
  <ITEM>μιας</ITEM>
  <ITEM>ενός</ITEM>
 </ROW>

 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>ένα(ν)</ITEM>
  <ITEM>μια</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>

</TABULAR>
</SECTION>

</PAGE>

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: polish-slash -->
<!-- alternative-dictionary: "polish" -->
<!-- End: -->

