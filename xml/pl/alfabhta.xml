<!--

  Copyright (C) 2006 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<PAGE PREFIX="alfabhta">
<HEADER>Pismo</HEADER>

<SECTION>
<HEADER>Alfabet</HEADER>
<PARA>Poniższa tabela opisuje alfabet grecki.  Wymowa podaje się
w <DFN>międzynarodowej transkrypcji fonetycznej
(IPA)</DFN>. Jeśli istnieje fonetyczny odpowiednik w języku polskim,
podaje się go w czwartej kolumnie. Szczegóły wymowy dźwięków i ich
połączeń omawiają się w następnym rozdziale.
</PARA>

<TABULAR COLHEADING="std">
<ROW>
 <ITEM>Znak</ITEM>
 <ITEM>Nazwa</ITEM>
 <ITEM>IPA</ITEM>
 <ITEM>Polski</ITEM>

 <ITEM>Znak</ITEM>
 <ITEM>Nazwa</ITEM>
 <ITEM>IPA</ITEM>
 <ITEM>Polski</ITEM>
</ROW>
<ROW>
 <ITEM>α Α</ITEM>
 <ITEM>άλφα  </ITEM>
 <ITEM>ɑ</ITEM>
 <ITEM>а</ITEM>

 <ITEM>ν  Ν </ITEM>
 <ITEM>νυ</ITEM>
 <ITEM>n </ITEM>
 <ITEM>n </ITEM>
</ROW>
<ROW>
 <ITEM>β Β</ITEM>
 <ITEM>βήτα</ITEM>
 <ITEM>v</ITEM>
 <ITEM>w</ITEM>

 <ITEM>ξ  Ξ </ITEM>
 <ITEM>ξι</ITEM>
 <ITEM>ks</ITEM>
 <ITEM>ks</ITEM>
</ROW>
<ROW>
 <ITEM>γ Γ</ITEM>
 <ITEM>γάμμα</ITEM>
 <ITEM>ɤ</ITEM>
 <ITEM>h<XREF REF="gamma"><SUPERSCRIPT>(I)</SUPERSCRIPT></XREF></ITEM>

 <ITEM>ο  Ο </ITEM>
 <ITEM>όμικρον</ITEM>
 <ITEM>o </ITEM>
 <ITEM>o </ITEM>
</ROW>
<ROW>
 <ITEM>δ Δ</ITEM>
 <ITEM>δέλτα </ITEM>
 <ITEM>ð</ITEM>
 <ITEM><XREF REF="delta"><SUPERSCRIPT>(II)</SUPERSCRIPT></XREF></ITEM>

 <ITEM>π  Π </ITEM>
 <ITEM>πι   </ITEM>
 <ITEM>p </ITEM>
 <ITEM>p </ITEM>
 
</ROW>
<ROW>
 <ITEM>ε Ε</ITEM>
 <ITEM>έψιλον</ITEM>
 <ITEM>ɛ</ITEM>
 <ITEM>e</ITEM>

 <ITEM>ρ  Ρ </ITEM>
 <ITEM>ρω</ITEM>
 <ITEM>r </ITEM>
 <ITEM>r </ITEM>

</ROW>
<ROW>
 <ITEM>ζ Ζ</ITEM>
 <ITEM>ζήτα</ITEM>
 <ITEM>z</ITEM>
 <ITEM>z</ITEM>

 <ITEM>σ,ς<FOOTNOTE>Znaku <SAMP>σ</SAMP> używa się na początku i w
środku słowa, <SAMP>ς</SAMP> pisze się na końcu słowa.</FOOTNOTE> Σ</ITEM>
 <ITEM>σιγμα</ITEM>
 <ITEM>s</ITEM>
 <ITEM>s <XREF REF="sigma"><SUPERSCRIPT>(V)</SUPERSCRIPT></XREF></ITEM>
</ROW>
<ROW>
 <ITEM>η Η</ITEM>
 <ITEM>ήτα</ITEM>
 <ITEM>i</ITEM>
 <ITEM>i</ITEM>

 <ITEM>τ   Τ</ITEM>
 <ITEM>ταυ</ITEM>
 <ITEM>t </ITEM>
 <ITEM>t </ITEM>
 
</ROW>
<ROW>
 <ITEM>θ Θ</ITEM>
 <ITEM>θήτα</ITEM>
 <ITEM>θ</ITEM>
 <ITEM><XREF REF="thita"><SUPERSCRIPT>(III)</SUPERSCRIPT></XREF> </ITEM>

 <ITEM>υ  Υ </ITEM>
 <ITEM>ύψιλον </ITEM>
 <ITEM>i </ITEM>
 <ITEM>i </ITEM>
 
</ROW>
<ROW>
 <ITEM>ι Ι</ITEM>
 <ITEM>ιώτα</ITEM>
 <ITEM>i</ITEM>
 <ITEM>i</ITEM>

 <ITEM>φ  Φ </ITEM>
 <ITEM>φι   </ITEM>
 <ITEM>f </ITEM>
 <ITEM>f </ITEM>
 
</ROW>
<ROW>
 <ITEM>κ Κ</ITEM>
 <ITEM>κάππα</ITEM>
 <ITEM>k</ITEM>
 <ITEM>k</ITEM>

 <ITEM>χ  Χ </ITEM>
 <ITEM>χι   </ITEM>
 <ITEM>x </ITEM>
 <ITEM>ch </ITEM>
 
</ROW>
<ROW>
 <ITEM>λ Λ</ITEM>
 <ITEM>λάμδα</ITEM>
 <ITEM>l</ITEM>
 <ITEM>l</ITEM>

 <ITEM>ψ Ψ</ITEM>
 <ITEM>ψι   </ITEM>
 <ITEM>ps</ITEM>
 <ITEM>ps</ITEM>
 
</ROW>
<ROW>
 <ITEM>μ Μ</ITEM>
 <ITEM>μυ</ITEM>
 <ITEM>m</ITEM>
 <ITEM>m</ITEM>

 <ITEM>ω Ω</ITEM>
 <ITEM>ωμέγα</ITEM>
 <ITEM>o </ITEM>
 <ITEM>o </ITEM>
 
</ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Zapis liczb</HEADER>
<PARA>Każda litera greckiego alfabetu posiada również znaczenie
liczbowe. Ten system zapisu liczb nazywany jest <DFN>miletskim</DFN>
(<DFN>μιλησιακό σύστημα αρίθμησης</DFN>). Używa się go obecnie do
reprezentacji liczebników porządkowych, numeracji rozdziałów w
książkach i w sytuacjach analogicznych do stosowania rzymskiego
zapisu w kulturze zachodniej.</PARA> 
  
<PARA>Zachodni system liczbowy jest systemem pozycyjnym, tzn.
liczbowe znaczenie cyfry zależy od jej pozycji w liczbie, tak że
<SAMP>1</SAMP> to jeden, <SAMP>10</SAMP> to dziesięć i td. Na odmianę
od tego, grecki system liczbowy jest systemem addytywnym, w którym
każda litera posiada stałe znaczenie liczbowe, znaczenie zaś liczby
otrzymuje się przez zsumowanie wartości odpowiadającej każdej literze.
</PARA>
<PARA>System ten powstał w IV w. p.n.e., kiedy to alfabet posiadał
więcej liter niż obecnie. Cztery z tych archaicznych liter są wciąż
używane dla zapisywania liczb.  Są to:
</PARA>  

<TABULAR COLHEADING="std">
  <ROW>
    <ITEM>Litera</ITEM>
    <ITEM>Nazwa</ITEM>
  </ROW>
  <ROW>
    <ITEM>ϝ Ϝ</ITEM>
    <ITEM>Δίγαμμα</ITEM>
  </ROW>
  <ROW>
    <ITEM>ϛ Ϛ</ITEM>
    <ITEM>Στίγμα</ITEM>
  </ROW>
  <ROW>
    <ITEM>Ϟ ϟ</ITEM>
    <ITEM>Κόππα</ITEM>
  </ROW>
  <ROW>
    <ITEM>ϡ Ϡ</ITEM>
    <ITEM>Σαμπί</ITEM>
  </ROW>
</TABULAR>

<PARA>Literα <SAMP>ϛ</SAMP> (<DFN>stigma</DFN>) mimo podobieństwa do
<SAMP>ς</SAMP> (sigma końcowa) nie jest z nią tożsama.</PARA>

<PARA>Znaczenia liczbowe liter greckiego alfabetu są:</PARA>

<TABULAR COLHEADING="std">
<ROW>
 <ITEM>Znak</ITEM>
 <ITEM>Znaczenie</ITEM>

 <ITEM>Znak</ITEM>
 <ITEM>Znaczenie</ITEM>

 <ITEM>Znak</ITEM>
 <ITEM>Znaczenie</ITEM>
</ROW>
<ROW>
 <ITEM>α Α</ITEM>
 <ITEM>1</ITEM>
 <ITEM>ι Ι</ITEM>
 <ITEM>10</ITEM>
 <ITEM>ρ Ρ</ITEM>
 <ITEM>100</ITEM>
</ROW>
<ROW>
 <ITEM>β Β</ITEM>
 <ITEM>2</ITEM>
 <ITEM>κ Κ</ITEM>
 <ITEM>20</ITEM>
 <ITEM>σ Σ</ITEM>
 <ITEM>200</ITEM>
</ROW>
<ROW>
 <ITEM>γ Γ</ITEM>
 <ITEM>3</ITEM>
 <ITEM>λ Λ</ITEM>
 <ITEM>30</ITEM>
 <ITEM>τ Τ</ITEM>
 <ITEM>300</ITEM>
</ROW> 
<ROW>
 <ITEM>δ Δ</ITEM>
 <ITEM>4</ITEM>
 <ITEM>μ Μ</ITEM>
 <ITEM>40</ITEM>
 <ITEM>υ  Υ</ITEM>
 <ITEM>400</ITEM>
</ROW>
<ROW>
 <ITEM>ε Ε</ITEM>
 <ITEM>5</ITEM>
 <ITEM>ν Ν</ITEM>
 <ITEM>50</ITEM>
 <ITEM>φ Φ</ITEM>
 <ITEM>500</ITEM>
</ROW>
<ROW>
 <ITEM>ϝ Ϝ, ϛ Ϛ, στ</ITEM> 
 <ITEM>6</ITEM>
 <ITEM>ξ Ξ</ITEM>
 <ITEM>60</ITEM>
 <ITEM>χ Χ</ITEM> 
 <ITEM>600</ITEM>
</ROW> 
<ROW>
 <ITEM>ζ Ζ</ITEM>
 <ITEM>7</ITEM>
 <ITEM>ο Ο</ITEM>
 <ITEM>70</ITEM>
 <ITEM>ψ Ψ</ITEM>
 <ITEM>700</ITEM>
</ROW> 
<ROW>
 <ITEM>η Η</ITEM>
 <ITEM>8</ITEM>
 <ITEM>π Π</ITEM>
 <ITEM>80</ITEM>
 <ITEM>ω Ω</ITEM>
 <ITEM>800</ITEM>
</ROW>
<ROW>
 <ITEM>θ Θ</ITEM>
 <ITEM>9</ITEM>
 <ITEM>Ϟ ϟ</ITEM>
 <ITEM>90</ITEM>
 <ITEM>ϡ Ϡ</ITEM>
 <ITEM>900</ITEM>
</ROW>
</TABULAR>

<PARA>Jak widać litery <SAMP>ϝ</SAMP> oraz <SAMP>ϛ</SAMP> mają
jednakowe znaczenia liczbowe.  Ponadto, zamiast nich może występować
kombinacja <SAMP>στ</SAMP>.</PARA>

<PARA>Żeby odróżnić liczbę od słowa lub kombinacji liter, liczba jest
zawsze zakończona symbolem <SAMP>ʹ</SAMP>,
nazywanym <SAMP>κεραία</SAMP>.</PARA>

<PARA>Na przykład, liczbę 365 w systemie greckim zapisujemy jak
następuje: <SAMP>τξεʹ</SAMP>.</PARA>

<PARA>Litery od <SAMP>α</SAMP> do <SAMP>θ</SAMP> poprzedzone
znakiem <SAMP>͵</SAMP> (<SAMP>αριστερή κεραία</SAMP>) przedstawiają
znaczenia od 1000 do 9999:</PARA>

<TABULAR COLHEADING="std">
<ROW>
 <ITEM>Znak</ITEM>
 <ITEM>Znaczenie</ITEM>
</ROW>
<ROW>
 <ITEM>͵α ͵Α</ITEM>
 <ITEM>1000</ITEM>
</ROW>
<ROW>
 <ITEM>͵β ͵Β</ITEM>
 <ITEM>2000</ITEM>
</ROW>
<ROW>
 <ITEM>͵γ ͵Γ</ITEM>
 <ITEM>3000</ITEM>
</ROW> 
<ROW>
 <ITEM>͵δ ͵Δ</ITEM>
 <ITEM>4000</ITEM>
</ROW>
<ROW>
 <ITEM>͵ε ͵Ε</ITEM>
 <ITEM>5000</ITEM>
</ROW>
<ROW>
 <ITEM>͵ϝ ͵Ϝ, ͵ϛ ͵Ϛ</ITEM> 
 <ITEM>6000</ITEM>
</ROW> 
<ROW>
 <ITEM>͵ζ ͵Ζ</ITEM>
 <ITEM>7000</ITEM>
</ROW> 
<ROW>
 <ITEM>͵η ͵Η</ITEM>
 <ITEM>8000</ITEM>
</ROW>
<ROW>
 <ITEM>͵θ ͵Θ</ITEM>
 <ITEM>9000</ITEM>
</ROW>
</TABULAR>

<PARA>Na przykład <SAMP>2011</SAMP>
zapisujemy jako <SAMP>͵βιαʹ</SAMP>.</PARA>   

<PARA>Liczby większe od 9999 zapisywano przy pomocy
litery <SAMP>Μ</SAMP> oznaczającej <SAMP>μυριάς</SAMP>, czyli 10000.
Nad tą literą zapisywano liczbę przez którą należało pomnożyć 10000
aby otrzymać pożądaną liczbę.  Na przykład:</PARA>

<IMAGE SRC="arith.png" />
  
  
  
</SECTION>
</PAGE> 



<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: polish-slash -->
<!-- alternative-dictionary: "polish" -->
<!-- End: -->
