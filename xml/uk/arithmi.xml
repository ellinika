<!-- 

  Copyright (C) 2004,2006 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<CHAPTER PREFIX="arithmi">
<PAGE HEADER="Числівник">
<PARA>
Новогрецька мова має кількісні і порядкові числівники, та
числівникові прислівники.
</PARA>
<PARA>
Розділові числівники утворюються описово:
</PARA>

<TABULAR BESTFIT="1" NOFRAME="1">
<ROW>
 <ITEM>ένας ένας</ITEM> <ITEM>по одному</ITEM>
</ROW>
<ROW>
 <ITEM>ανά δύο</ITEM>   <ITEM>по двоє</ITEM>
</ROW>
<ROW>
 <ITEM>ανά τρία</ITEM>  <ITEM>по троє</ITEM>
</ROW>
</TABULAR>

<PARA>
Із кількісних числівників відмінюються: <EXAMPLE>один</EXAMPLE>
(<DICTREF>ένας</DICTREF>), <EXAMPLE>три</EXAMPLE>
(<DICTREF>τρία</DICTREF>), <EXAMPLE>чотири</EXAMPLE>
(<DICTREF>τέσσερα</DICTREF>), 
<EXAMPLE>тисяча</EXAMPLE>
(<DICTREF>χίλια</DICTREF>), а також всі сотні.
</PARA>

<PARA>
Порядкові числівники відмінюються як прикметники на
<FLECT SPLIT=",">ος, η, ο</FLECT>.
</PARA>


</PAGE>

<PAGE HEADER="Кількісні числівники">
<ANCHOR ID="apolyta" />
<TABULAR OR="," ALTERNATE="1" COLHEADING="std">
<ROW><ITEM>Значення</ITEM><ITEM>Числівник</ITEM></ROW>
<ROW><ITEM>0</ITEM> <ITEM>  μηδέν</ITEM></ROW>
<ROW><ITEM>1</ITEM> <ITEM>  ένας, μια, ένα</ITEM></ROW>
<ROW><ITEM>2</ITEM> <ITEM>  δύο</ITEM></ROW>
<ROW><ITEM>3</ITEM> <ITEM>  τρεις, τρεις, τρία</ITEM></ROW>
<ROW><ITEM>4</ITEM> <ITEM>  τέσσερις, τέσσερις, τέσσερα</ITEM></ROW>
<ROW><ITEM>5</ITEM> <ITEM>  πέντε</ITEM></ROW>
<ROW><ITEM>6</ITEM> <ITEM>  έξι</ITEM></ROW>
<ROW><ITEM>7</ITEM> <ITEM>  επτά/εφτά</ITEM></ROW>
<ROW><ITEM>8</ITEM> <ITEM>  οκτώ/οχτώ</ITEM></ROW>
<ROW><ITEM>9</ITEM> <ITEM>  εννέα/εννιά</ITEM></ROW>
<ROW><ITEM>10</ITEM><ITEM>  δέκα</ITEM></ROW>
<ROW><ITEM>11</ITEM><ITEM>  ένδεκα</ITEM></ROW>
<ROW><ITEM>12</ITEM><ITEM>  δώδεκα</ITEM></ROW>
<ROW><ITEM>13</ITEM><ITEM>  δεκατρία</ITEM></ROW>
<ROW><ITEM>14</ITEM><ITEM>  δεκατέσσερα</ITEM></ROW>
<ROW><ITEM>15</ITEM><ITEM>  δεκαπέντε</ITEM></ROW>
<ROW><ITEM>16</ITEM><ITEM>  δεκαέξι</ITEM></ROW>
<ROW><ITEM>17</ITEM><ITEM>  δεκαεπτά/δεκαεφτά</ITEM></ROW>
<ROW><ITEM>18</ITEM><ITEM>  δεκαοκτώ/δεκαοχτώ</ITEM></ROW>
<ROW><ITEM>19</ITEM><ITEM>  δεκαεννέα/δεκαεννιά</ITEM></ROW>
<ROW><ITEM>20</ITEM><ITEM>  είκοσι</ITEM></ROW>
<ROW><ITEM>21</ITEM><ITEM>  είκοσι ένα</ITEM></ROW>
<ROW><ITEM>22</ITEM><ITEM>  είκοσι δύο</ITEM></ROW>
<ROW><ITEM>23</ITEM><ITEM>  είκοσι τρία</ITEM></ROW>
	     
<ROW><ITEM>30</ITEM><ITEM>  τριάντα</ITEM></ROW>
<ROW><ITEM>31</ITEM><ITEM>  τριάντα ένα</ITEM></ROW>
<ROW><ITEM>40</ITEM><ITEM>  σαράντα</ITEM></ROW>
<ROW><ITEM>50</ITEM><ITEM>  πενήντα</ITEM></ROW>
<ROW><ITEM>60</ITEM><ITEM>  εξήντα</ITEM></ROW>
<ROW><ITEM>70</ITEM><ITEM>  εβδομήντα</ITEM></ROW>
<ROW><ITEM>80</ITEM><ITEM>  ογδόντα</ITEM></ROW>
<ROW><ITEM>90</ITEM><ITEM>  ενενήντα</ITEM></ROW>
	     
<ROW><ITEM>100</ITEM><ITEM>  εκατό</ITEM></ROW>
<ROW><ITEM>101</ITEM><ITEM>  εκατόν ένα</ITEM></ROW>
<ROW><ITEM>102</ITEM><ITEM>  εκατόν δύο</ITEM></ROW>

<ROW><ITEM>200</ITEM><ITEM>  διακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>300</ITEM><ITEM>  τριακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>400</ITEM><ITEM>  τετρακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>500</ITEM><ITEM>  πεντακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>600</ITEM><ITEM>  εξακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>700</ITEM><ITEM>  επτακόσιοι, -ες, -α / εφτακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>800</ITEM><ITEM>  οκτακόσιοι, -ες, -α / οχτακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>900</ITEM><ITEM>  εννιακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>1000</ITEM><ITEM> χίλια</ITEM></ROW>
<ROW><ITEM>2000</ITEM><ITEM> δύο χιλιάδες</ITEM></ROW>
<ROW><ITEM>3000</ITEM><ITEM> τρεις χιλιάδες</ITEM></ROW>
<ROW><ITEM>4000</ITEM><ITEM> τέσσερις χιλιάδες</ITEM></ROW>

<ROW><ITEM>1.000.000</ITEM><ITEM>ένα εκατομμύριο</ITEM></ROW>
<ROW><ITEM>1.000.000.000</ITEM><ITEM>ένα δισεκατομμύριο</ITEM></ROW>
<ROW><ITEM>1.000.000.000.000</ITEM><ITEM>ένα τρισεκατομμύριο</ITEM></ROW>
</TABULAR>

<SECTION>
<HEADER>Відмінювання числівника ένας</HEADER>

<TABULAR ALTERNATE="1" ROWHEADING="std">
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>ένας</ITEM>
  <ITEM>μία</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>ένα(ν)</ITEM>
  <ITEM>μία</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>ενός</ITEM>
  <ITEM>μίας</ITEM>
  <ITEM>ενός</ITEM>
 </ROW>
</TABULAR>

<PARA>
На відміну від <XREF REF="aoristo_arthro">неозначеного
артикля</XREF>, що має лиш один склад  (<SAMP>μια</SAMP>), жіночий рід
числівника <IT>один</IT> має два склади (<SAMP>μία</SAMP>).
</PARA>
<PARA>
Αγόρασα μόνο <EMPH>μία</EMPH> ζακέτα.
Я купив лиш <EMPH>один</EMPH> жакет. (числівник)
</PARA>
<PARA>
<EMPH>Μια</EMPH> φορά ήταν ένας βασιλιάς. Колись давно (букв. "одного разу") жив-був цар. (артикль)
</PARA>
</SECTION>

<SECTION>
<HEADER>Відмінювання числівників τρεις и τέσσερις</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std">
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρία</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρία</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τριών</ITEM>
  <ITEM>τριών</ITEM>
  <ITEM>τριών</ITEM>
 </ROW>
 <SEPARATOR></SEPARATOR>
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τεσσάρων</ITEM>
  <ITEM>τεσσάρων</ITEM>
  <ITEM>τεσσάρων</ITEM>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Відмінювання числівника χίλια</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std">
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>χίλιοι</ITEM>
  <ITEM>χίλιες</ITEM>
  <ITEM>χίλια</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>χίλιους</ITEM>
  <ITEM>χίλιες</ITEM>
  <ITEM>χίλια</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>χιλίων</ITEM>
  <ITEM>χιλίων</ITEM>
  <ITEM>χιλίων</ITEM>
 </ROW>
</TABULAR>

<PARA>
 Числівник χίλια приймає форму χιλιάδες, якщо кількість тисяч більше однієї.
</PARA>

<PARA>Поняття <TRANS>обидва</TRANS> передається виразом
<EXAMPLE>οι δύο, τα δύο</EXAMPLE>.</PARA>

<KATHAREVUSA>
<PARA>
В кафаревусі <TRANS>обидва</TRANS> передається словом <EXAMPLE>ἀμφότεροι,‐αι,‐α</EXAMPLE>.
</PARA>
</KATHAREVUSA>
</SECTION>

<SECTION>
<HEADER>Узгодження числівників</HEADER>
<PARA>
Всі відмінювані числівники (одиниці, трійки, четвірки та сотні) у
першій групі узгоджуються з обчислюваними іменниками, а в кожній
наступній групі -  з числівником попередньої (тобто одиниці з
іменником, десятки з одиницями, сотні з десятками, тощо). Рід
та число всієї конструкції узгоджуються з родом та числом числівника
старшого порядку. Наприклад:

<TABULAR BESTFIT="1" NOFRAME="1">
<ROW>
 <ITEM>4 стільці</ITEM>
 <ITEM>οι τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>644 стільці</ITEM>
 <ITEM>οι εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>1.644 стільці</ITEM>
 <ITEM>οι χίλιες εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>533.644 стільці</ITEM>
 <ITEM>οι πεντακόσιες τριάντα τρεις χιλιάδες εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>964.533.644 стільці</ITEM>
 <ITEM>τα εννιακόσια εξήντα τέσσερα εκατομμύρια πεντακόσιες τριάντα τρεις χιλιάδες εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>4 книги</ITEM>
 <ITEM>τα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>644 книги</ITEM>
 <ITEM>τα εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>1.644 книги</ITEM>
 <ITEM>τα χίλια εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>533.644 книги</ITEM>
 <ITEM>οι πεντακόσιες τριάντα τρεις χιλιάδες εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>964.533.644 книги</ITEM>
 <ITEM>τα εννιακόσια εξήντα τέσσερα εκατομμύρια πεντακόσιες τριάντα τρεις χιλιάδες εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
</TABULAR>

</PARA>
</SECTION>

<SECTION>
<HEADER>Арифметичні дії</HEADER>
<PARA>
Для лічби та в математиці використовуються форми числівника
середнього роду.
</PARA>

<PARA>Арифметичні дії передаються наступним чином:</PARA>

<TABULAR NOFRAME="1">
<ROW>
 <ITEM>2 + 2 = 4</ITEM>
 <ITEM>δύο <DICTREF>συν</DICTREF> δύο <DICTREF>ίσον</DICTREF> (або κάνουν) τέσσερα</ITEM>
</ROW>

<ROW>
 <ITEM>5 - 3 = 2</ITEM>
 <ITEM>πέντε <DICTREF>μείον</DICTREF> (або πλην) τρία ίσον δύο</ITEM>
</ROW>

<ROW>
 <ITEM>7 x 4 = 28</ITEM>
 <ITEM>επτά <DICTREF>επί</DICTREF> τέσσερα δίνουν είκοσι οκτώ або
       επτά οι τέσσερες δίνουν είκοσι οκτώ</ITEM>
</ROW>

<ROW> 
 <ITEM>10 : 2 = 5</ITEM>
 <ITEM>δέκα <DICTREF>διά</DICTREF> δύο ίσον πέντε</ITEM>
</ROW>
</TABULAR>

<ANCHOR ID="toisekato" />
<PARA>
Число, що позначає відсоток, читається так: 15% - <EXAMPLE>δέκα τοις
εκατό</EXAMPLE>. (<EXAMPLE>τοις</EXAMPLE> - давальний відмінок
<XREF REF="k_arthra">означеного артикля</XREF> кафаревуси.)
</PARA>

</SECTION>

</PAGE>

<PAGE HEADER="Порядкові числівники">

<PARA>
Порядкові числівники відмінюються як
<XREF REF="epitheta:os:h:o">прикметники на
<FLECT SPLIT=",">ος, η, ο</FLECT></XREF>. Нижче в таблиці наведені
форми чоловічого роду порядкових числівників:
</PARA>

<ANCHOR ID="taktika" />
<TABULAR ALTERNATE="1" COLHEADING="std">
<ROW> <ITEM></ITEM>    <ITEM>Кількісний</ITEM> <ITEM>Порядковий</ITEM> </ROW>
<SEPARATOR>Одиниці</SEPARATOR>
<ROW> <ITEM>1</ITEM>   <ITEM> ένα</ITEM>         <ITEM>πρώτος</ITEM></ROW>
<ROW> <ITEM>2</ITEM>   <ITEM> δύο</ITEM>         <ITEM>δεύτερος</ITEM></ROW>
<ROW> <ITEM>3</ITEM>   <ITEM> τρία</ITEM>        <ITEM>τρίτος </ITEM></ROW>
<ROW> <ITEM>4</ITEM>   <ITEM> τέσσερα</ITEM>     <ITEM>τέταρτος </ITEM></ROW>
<ROW> <ITEM>5</ITEM>   <ITEM> πέντε</ITEM>       <ITEM>πέμπτος </ITEM></ROW>
<ROW> <ITEM>6</ITEM>   <ITEM> έξι</ITEM>         <ITEM>έκτος </ITEM></ROW>
<ROW> <ITEM>7</ITEM>   <ITEM> επτά/εφτά</ITEM>   <ITEM>έβδομος </ITEM></ROW>
<ROW> <ITEM>8</ITEM>   <ITEM> οκτώ/οχτώ</ITEM>   <ITEM>όγδοος </ITEM></ROW>
<ROW> <ITEM>9</ITEM>   <ITEM> εννέα/εννιά</ITEM> <ITEM>ένατος </ITEM></ROW>
<SEPARATOR>Перший десяток</SEPARATOR>
<ROW> <ITEM>10</ITEM>  <ITEM> δέκα</ITEM>        <ITEM>δέκατος </ITEM></ROW>
<ROW> <ITEM>11</ITEM>  <ITEM> ένδεκα</ITEM>      <ITEM>ενδέκατος </ITEM></ROW>
<ROW> <ITEM>12</ITEM>  <ITEM> δώδεκα</ITEM>      <ITEM>δωδέκατος </ITEM></ROW>
<ROW> <ITEM>13</ITEM>  <ITEM> δεκατρία</ITEM>    <ITEM>δέκατος τρίτος </ITEM></ROW>
<ROW> <ITEM>14</ITEM>  <ITEM> δεκατέσσερα</ITEM> <ITEM>δέκατος τέταρτος </ITEM></ROW>
<SEPARATOR>Десятки</SEPARATOR>
<ROW> <ITEM>20</ITEM>  <ITEM> είκοσι</ITEM>      <ITEM>εικοστός </ITEM></ROW>
<ROW> <ITEM>30</ITEM>  <ITEM> τριάντα</ITEM>     <ITEM>τριακοστός</ITEM></ROW>
<ROW> <ITEM>40</ITEM>  <ITEM> σαράντα</ITEM>     <ITEM>τετρακοστός, σαρακοστός</ITEM></ROW>
<ROW> <ITEM>50</ITEM>  <ITEM> πενήντα</ITEM>     <ITEM>πεντηκοστός</ITEM></ROW>
<ROW> <ITEM>60</ITEM>  <ITEM> εξήντα</ITEM>      <ITEM>εξηκοστός </ITEM></ROW>
<ROW> <ITEM>70</ITEM>  <ITEM> εβδομήντα</ITEM>   <ITEM>εβδομηκοστός </ITEM></ROW>
<ROW> <ITEM>80</ITEM>  <ITEM> ογδόντα</ITEM>     <ITEM>ογδοηκοστός</ITEM></ROW>
<ROW> <ITEM>90</ITEM>  <ITEM> ενενήντα</ITEM>    <ITEM>εννενηκοστός </ITEM></ROW>
	     
<SEPARATOR>Сотні</SEPARATOR>
<ROW><ITEM>100</ITEM><ITEM>  εκατό</ITEM>        <ITEM>εκατοστός</ITEM></ROW>
</TABULAR>
</PAGE>

<PAGE HEADER="Числівникові прислівники">
<PARA>Числівникові прислівники вказують, скільки разів відбулася
  якась дія (порівн. укр. <IT>один раз</IT>, <IT>двічі</IT>, <IT>тричі</IT>). У
  грецькій мові числівникові прислівники звичайно утворюються описово,
  за допомогою іменника <DICTREF>φορά</DICTREF>:
<EXAMPLE>μια φορά</EXAMPLE> <TRANS>один раз</TRANS>,
<EXAMPLE>δύο φορές</EXAMPLE> <TRANS>двічі</TRANS>, тощо.
</PARA>

<KATHAREVUSA>
<PARA>

В кафаревусі існують синтетичні форми, що утворюються за допомогою суфікса <FLECT>άκις</FLECT>.
</PARA>

<TABULAR ALTERNATE="1" COLHEADING="std">
<ROW>
 <ITEM>Прислівник</ITEM>
 <ITEM>Значення</ITEM>
</ROW>
<SEPARATOR>1-10</SEPARATOR>
<ROW>
 <ITEM></ITEM>
 <ITEM>один раз</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>двічі</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>тричі</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>чотири рази</ITEM>
</ROW>
<ROW>
 <ITEM>πεντάκις</ITEM>
 <ITEM>пять разів</ITEM>
</ROW>
<ROW>
 <ITEM>εξάκις</ITEM>
 <ITEM>шість разів</ITEM>
</ROW>
<ROW>
 <ITEM>επτάκις</ITEM>
 <ITEM>сім разів</ITEM>
</ROW>
<ROW>
 <ITEM>οκτάκις</ITEM>
 <ITEM>вісім разів</ITEM>
</ROW>
<ROW>
 <ITEM>εννεάκις</ITEM>
 <ITEM>дев'ять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκάκις</ITEM>
 <ITEM>десять разів</ITEM>
</ROW>
<SEPARATOR>11-19</SEPARATOR>
<ROW>
 <ITEM>ενδεκάκις</ITEM>
 <ITEM>одинадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δωδεκάκις</ITEM>
 <ITEM>дванадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκατριάκις</ITEM>
 <ITEM>тринадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκατετράκις</ITEM>
 <ITEM>чотирнадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαπεντάκις</ITEM>
 <ITEM>п'ятнадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαεξάκις</ITEM>
 <ITEM>шістнадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαεπτάκις</ITEM>
 <ITEM>сімнадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαοκτάκις</ITEM>
 <ITEM>вісімнадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαεννεάκις</ITEM>
 <ITEM>дев'ятнадцять разів</ITEM>
</ROW>
<SEPARATOR>20-90</SEPARATOR>
<ROW>
 <ITEM>εικοσάκις</ITEM>
 <ITEM>двадцять разів</ITEM>
</ROW>
<ROW>
 <ITEM>τριακοντάκις</ITEM>
 <ITEM>тридцять разів</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>сорок разів</ITEM>
</ROW>
<ROW>
 <ITEM>πεντηκοντάκις</ITEM>
 <ITEM>п'ятдесят разів</ITEM>
</ROW>
<ROW>
 <ITEM>εξηκοντάκις</ITEM>
 <ITEM>шістдесят разів</ITEM>
</ROW>
<ROW>
 <ITEM>εβδομηκονδάκις</ITEM>
 <ITEM>сімдесят разів</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>вісімдесят разів</ITEM>
</ROW>
<ROW>
 <ITEM>ενενηκοντάκις</ITEM>
 <ITEM>дев'яносто разів</ITEM>
</ROW>

<SEPARATOR>100-900</SEPARATOR>
<ROW>
 <ITEM>εκατοκονδάκις</ITEM>
 <ITEM>сто разів</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>двісті разів</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>триста разів</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>чотириста разів</ITEM>
</ROW>
<ROW>
 <ITEM>πεντακοσιάκις</ITEM>
 <ITEM>п'ятсот разів</ITEM>
</ROW>
<ROW>
 <ITEM>εξακοσιάκις</ITEM>
 <ITEM>шістсот разів</ITEM>
</ROW>
<ROW>
 <ITEM>επτακοσιάκις</ITEM>
 <ITEM>сімсот разів</ITEM>
</ROW>
<ROW>
 <ITEM>οκτακοσιάκις</ITEM>
 <ITEM>вісімсот разів</ITEM>
</ROW>
<ROW>
 <ITEM>εννεακοσιάκις</ITEM>
 <ITEM>дев'ятсот разів</ITEM>
</ROW>
</TABULAR>
</KATHAREVUSA>

</PAGE>

<PAGE>
 <HEADER>Дріб</HEADER>
<PARA>
Дріб утворюється таким чином: чисельник (ο αριθμητής)
передається <XREF REF="apolyta">кількісним числівником</XREF>
середнього роду, а знаменник (ο παρονομαστής) --
<XREF REF="taktika">порядковим числівником</XREF> середнього роду (де
мається на увазі слово <DICTREF>μέρος</DICTREF>):

<TABULAR BESTFIT="1" NOFRAME="1">
<ROW>
<ITEM>1/4</ITEM>
<ITEM>ένα τέταρτο</ITEM>
</ROW>

<ROW>
<ITEM>3/4</ITEM>
<ITEM>τρία τέταρτα</ITEM>
</ROW>

<ROW>
<ITEM>7/8</ITEM>
<ITEM>επτά όγδοα</ITEM>
</ROW>

<ROW>
<ITEM>1/100</ITEM>
<ITEM>ένα εκατοστό</ITEM>
</ROW>

<ROW>
<ITEM>1/1000</ITEM>
<ITEM>ένα χιλιοστό</ITEM>
</ROW>

</TABULAR>

</PARA> 
</PAGE>

</CHAPTER>

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: ukrainian-computer -->
<!-- alternative-dictionary: "ukrainian" -->
<!-- End: -->

