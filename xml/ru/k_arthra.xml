<!--

  Copyright (C) 2004 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<PAGE PREFIX="k_arthra" HEADER="Артикль">

<SECTION ID="k_arthra">
 <HEADER>Склонение определенного артикля</HEADER>
 
<PARA>В кафаревусе используется только определённый артикль. Его
склонение приведено в следующей таблице:</PARA>

<TABULAR ALTERNATE="1" ROWHEADING="std">
 <TITLE></TITLE>
 <ROW>
  <ITEM>Ὀνομαστική</ITEM>
  <ITEM>ὁ</ITEM>
  <ITEM>ἡ</ITEM>
  <ITEM>τό</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>τοῦ</ITEM>
  <ITEM>τῆς</ITEM>
  <ITEM>τοῦ</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αἰτιατική</ITEM>
  <ITEM>τόν</ITEM>
  <ITEM>τήν</ITEM>
  <ITEM>τό</ITEM>
 </ROW>

 <ROW>
  <ITEM>Δοτική</ITEM>
  <ITEM>τῶ, τῷ</ITEM>
  <ITEM>τῆ, τῇ</ITEM>
  <ITEM>τῶ, τῷ</ITEM>
 </ROW>

 <ROW>
  <ITEM>Ὀνομαστική</ITEM> 	
  <ITEM>οἱ</ITEM>
  <ITEM>αἱ</ITEM>
  <ITEM>τά</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM> 	
  <ITEM>τῶν</ITEM>
  <ITEM>τῶν</ITEM>
  <ITEM>τῶν</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αἰτιατική</ITEM>
  <ITEM>τούς</ITEM>
  <ITEM>τάς</ITEM>
  <ITEM>τά</ITEM>
 </ROW>

 <ROW>
  <ITEM>Δοτική</ITEM>
  <ITEM>τοῖς</ITEM>
  <ITEM>ταῖς</ITEM>
  <ITEM>τοῖς</ITEM>
 </ROW>
 
</TABULAR>

<PARA>Звательный падеж существительных может употреблятся с
междометием <EXAMPLE>ὧ</EXAMPLE>, которое иногда называют формой
звательного падежа определённого артикля.</PARA>

</SECTION>

<SECTION>
 <HEADER>Особенности употребления</HEADER>

<PARA>
Дополнительно к <XREF REF="oristiko_arthro">общим правилам</XREF>
использования определённого артикля следует заметить следующие
особенности:
</PARA>

<PARA>
Определённый артикль может использоваться самостоятельно
(при подразумеваемом существительном): <EXAMPLE>τὰ τῆς
πατρίδος</EXAMPLE> <TRANS>то, что относится к нашей родине</TRANS>
(подразумевается πράγματα), <EXAMPLE>οἱ περὶ αὐτόν</EXAMPLE>
<TRANS>окружающие его</TRANS> (подразумевается ἄνθρωποι).
</PARA>

<PARA>Артикль может использоваться не только при существительном, но и
при понятии, выраженном сложным сочетанием: <EXAMPLE>ἡ πρὸς τὴν
πατρίδα καὶ τὸν λαόν ἀγάπη</EXAMPLE> <TRANS>любовь к родине и
народу</TRANS>.
</PARA>

<PARA>
Определённый артикль встречается так же в роли
указательного местоимения в сочетании <EXAMPLE>ὁ μὲν ... ὁ δέ
...</EXAMPLE> <TRANS>один ... другой ...</TRANS>, например: <EXAMPLE>ὁ
μὲν ἧτο πλούσιος, ὁ δὲ πτωχός</EXAMPLE> <TRANS>один был богат, а
другой беден</TRANS>.
</PARA>

</SECTION>

</PAGE>

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: cyrillic-jcuken -->
<!-- alternative-dictionary: "russian" -->
<!-- End: -->
