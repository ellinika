<!-- 

  Copyright (C) 2004, 2007, 2010 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<CHAPTER PREFIX="arithmi">
<PAGE HEADER="Числительные">
<PARA>
В новогреческом языке есть числительные количественные и порядковые
и числительные наречия.
</PARA>
<PARA>
Числительные разделительные образуются описательно:
</PARA>

<TABULAR BESTFIT="1" NOFRAME="1">
<ROW>
 <ITEM>ένας ένας</ITEM> <ITEM>по одному</ITEM>
</ROW>
<ROW>
 <ITEM>ανά δύο</ITEM>   <ITEM>по два</ITEM>
</ROW>
<ROW>
 <ITEM>ανά τρία</ITEM>  <ITEM>по три</ITEM>
</ROW>
</TABULAR>

<PARA>
Из количественных числительных склоняются числительные <EXAMPLE>один</EXAMPLE>
(<DICTREF>ένας</DICTREF>), <EXAMPLE>три</EXAMPLE>
(<DICTREF>τρία</DICTREF>), <EXAMPLE>четыре</EXAMPLE>
(<DICTREF>τέσσερα</DICTREF>), 
<EXAMPLE>тысяча</EXAMPLE>
(<DICTREF>χίλια</DICTREF>) а также все сотни.
</PARA>

<PARA>
Порядковые числительные склоняются как прилагательные на <FLECT SPLIT=",">ος, η, ο</FLECT>.
</PARA>


</PAGE>

<PAGE HEADER="Количественные числительные">
<ANCHOR ID="apolyta" />
<TABULAR OR="," ALTERNATE="1" COLHEADING="std">
<ROW><ITEM>Значение</ITEM><ITEM>Числительное</ITEM></ROW>
<ROW><ITEM>0</ITEM> <ITEM>  μηδέν</ITEM></ROW>
<ROW><ITEM>1</ITEM> <ITEM>  ένας, μια, ένα</ITEM></ROW>
<ROW><ITEM>2</ITEM> <ITEM>  δύο</ITEM></ROW>
<ROW><ITEM>3</ITEM> <ITEM>  τρεις, τρεις, τρία</ITEM></ROW>
<ROW><ITEM>4</ITEM> <ITEM>  τέσσερις, τέσσερις, τέσσερα</ITEM></ROW>
<ROW><ITEM>5</ITEM> <ITEM>  πέντε</ITEM></ROW>
<ROW><ITEM>6</ITEM> <ITEM>  έξι</ITEM></ROW>
<ROW><ITEM>7</ITEM> <ITEM>  επτά/εφτά</ITEM></ROW>
<ROW><ITEM>8</ITEM> <ITEM>  οκτώ/οχτώ</ITEM></ROW>
<ROW><ITEM>9</ITEM> <ITEM>  εννέα/εννιά</ITEM></ROW>
<ROW><ITEM>10</ITEM><ITEM>  δέκα</ITEM></ROW>
<ROW><ITEM>11</ITEM><ITEM>  ένδεκα</ITEM></ROW>
<ROW><ITEM>12</ITEM><ITEM>  δώδεκα</ITEM></ROW>
<ROW><ITEM>13</ITEM><ITEM>  δεκατρία</ITEM></ROW>
<ROW><ITEM>14</ITEM><ITEM>  δεκατέσσερα</ITEM></ROW>
<ROW><ITEM>15</ITEM><ITEM>  δεκαπέντε</ITEM></ROW>
<ROW><ITEM>16</ITEM><ITEM>  δεκαέξι</ITEM></ROW>
<ROW><ITEM>17</ITEM><ITEM>  δεκαεπτά/δεκαεφτά</ITEM></ROW>
<ROW><ITEM>18</ITEM><ITEM>  δεκαοκτώ/δεκαοχτώ</ITEM></ROW>
<ROW><ITEM>19</ITEM><ITEM>  δεκαεννέα/δεκαεννιά</ITEM></ROW>
<ROW><ITEM>20</ITEM><ITEM>  είκοσι</ITEM></ROW>
<ROW><ITEM>21</ITEM><ITEM>  είκοσι ένα</ITEM></ROW>
<ROW><ITEM>22</ITEM><ITEM>  είκοσι δύο</ITEM></ROW>
<ROW><ITEM>23</ITEM><ITEM>  είκοσι τρία</ITEM></ROW>
	     
<ROW><ITEM>30</ITEM><ITEM>  τριάντα</ITEM></ROW>
<ROW><ITEM>31</ITEM><ITEM>  τριάντα ένα</ITEM></ROW>
<ROW><ITEM>40</ITEM><ITEM>  σαράντα</ITEM></ROW>
<ROW><ITEM>50</ITEM><ITEM>  πενήντα</ITEM></ROW>
<ROW><ITEM>60</ITEM><ITEM>  εξήντα</ITEM></ROW>
<ROW><ITEM>70</ITEM><ITEM>  εβδομήντα</ITEM></ROW>
<ROW><ITEM>80</ITEM><ITEM>  ογδόντα</ITEM></ROW>
<ROW><ITEM>90</ITEM><ITEM>  ενενήντα</ITEM></ROW>
	     
<ROW><ITEM>100</ITEM><ITEM>  εκατό</ITEM></ROW>
<ROW><ITEM>101</ITEM><ITEM>  εκατόν ένα</ITEM></ROW>
<ROW><ITEM>102</ITEM><ITEM>  εκατόν δύο</ITEM></ROW>

<ROW><ITEM>200</ITEM><ITEM>  διακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>300</ITEM><ITEM>  τριακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>400</ITEM><ITEM>  τετρακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>500</ITEM><ITEM>  πεντακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>600</ITEM><ITEM>  εξακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>700</ITEM><ITEM>  επτακόσιοι, -ες, -α / εφτακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>800</ITEM><ITEM>  οκτακόσιοι, -ες, -α / οχτακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>900</ITEM><ITEM>  εννιακόσιοι, -ες, -α</ITEM></ROW>
<ROW><ITEM>1000</ITEM><ITEM> χίλια</ITEM></ROW>
<ROW><ITEM>2000</ITEM><ITEM> δύο χιλιάδες</ITEM></ROW>
<ROW><ITEM>3000</ITEM><ITEM> τρεις χιλιάδες</ITEM></ROW>
<ROW><ITEM>4000</ITEM><ITEM> τέσσερις χιλιάδες</ITEM></ROW>

<ROW><ITEM>1.000.000</ITEM><ITEM>ένα εκατομμύριο</ITEM></ROW>
<ROW><ITEM>1.000.000.000</ITEM><ITEM>ένα δισεκατομμύριο</ITEM></ROW>
<ROW><ITEM>1.000.000.000.000</ITEM><ITEM>ένα τρισεκατομμύριο</ITEM></ROW>
</TABULAR>

<SECTION>
<HEADER>Склонение числительного ένας</HEADER>

<TABULAR ALTERNATE="1" ROWHEADING="std">
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>ένας</ITEM>
  <ITEM>μία</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>ένα(ν)</ITEM>
  <ITEM>μία</ITEM>
  <ITEM>ένα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>ενός</ITEM>
  <ITEM>μίας</ITEM>
  <ITEM>ενός</ITEM>
 </ROW>
</TABULAR>

<PARA>
Женский род числительного <IT>один</IT> имеет как правило два слога
(<SAMP>μία</SAMP>), в отличие от <XREF REF="aoristo_arthro">неопределённого
артикля</XREF>, в котором только один слог (<SAMP>μια</SAMP>).
</PARA>
<PARA>
Αγόρασα μόνο <EMPH>μία</EMPH> ζακέτα.
Я купил только <EMPH>один</EMPH> пиджак. (числительное)
</PARA>
<PARA>
<EMPH>Μια</EMPH> φορά ήταν ένας βασιλιάς. Однажды (букв. "некий раз") жил-был царь. (артикль)
</PARA>
</SECTION>

<SECTION>
<HEADER>Склонение числительных τρεις и τέσσερις</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std">
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρία</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρεις</ITEM>
  <ITEM>τρία</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τριών</ITEM>
  <ITEM>τριών</ITEM>
  <ITEM>τριών</ITEM>
 </ROW>
 <SEPARATOR></SEPARATOR>
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερις</ITEM>
  <ITEM>τέσσερα</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>τεσσάρων</ITEM>
  <ITEM>τεσσάρων</ITEM>
  <ITEM>τεσσάρων</ITEM>
 </ROW>
</TABULAR>
</SECTION>

<SECTION>
<HEADER>Склонение числительного χίλια</HEADER>
<TABULAR ALTERNATE="1" ROWHEADING="std">
 <ROW>
  <ITEM>Ονομαστική</ITEM>
  <ITEM>χίλιοι</ITEM>
  <ITEM>χίλιες</ITEM>
  <ITEM>χίλια</ITEM>
 </ROW>
 <ROW>
  <ITEM>Γενική</ITEM>
  <ITEM>χίλιους</ITEM>
  <ITEM>χίλιες</ITEM>
  <ITEM>χίλια</ITEM>
 </ROW>
 <ROW>
  <ITEM>Αιτιατική</ITEM>
  <ITEM>χιλίων</ITEM>
  <ITEM>χιλίων</ITEM>
  <ITEM>χιλίων</ITEM>
 </ROW>
</TABULAR>

<PARA>
Числительное χίλια принимает форму χιλιάδες, если количество тысяч больше одной.
</PARA>

<PARA>Понятие <TRANS>оба</TRANS> передаётся выражением
<EXAMPLE>οι δύο, τα δύο</EXAMPLE>.</PARA>

<KATHAREVUSA>
<PARA>
В кафаревусе <TRANS>оба</TRANS> передаётся словом <EXAMPLE>ἀμφότεροι,‐αι,‐α</EXAMPLE>.
</PARA>
</KATHAREVUSA>
</SECTION>

<SECTION>
<HEADER>Согласование числительных</HEADER>
<PARA>
 Все склоняемые числительные в каждой группе (т.е. сотни, единицы, тройки и
четвёрки) согласуются по роду и числу с числительным следующей группы или,
в первой группе, с исчисляемым существительным. Род и число всей конструкции
совпадает с родом и числом числительного старшего порядка. Например:

<TABULAR BESTFIT="1" NOFRAME="1">
<ROW>
 <ITEM>4 стула</ITEM>
 <ITEM>οι τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>644 стула</ITEM>
 <ITEM>οι εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>1.644 стула</ITEM>
 <ITEM>οι χίλιες εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>533.644 стула</ITEM>
 <ITEM>οι πεντακόσιες τριάντα τρεις χιλιάδες εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>964.533.644 стула</ITEM>
 <ITEM>τα εννιακόσια εξήντα τέσσερα εκατομμύρια πεντακόσιες τριάντα τρεις χιλιάδες εξακόσιες σαράντα τέσσερις καρέκλες</ITEM>
</ROW>
<ROW>
 <ITEM>4 книги</ITEM>
 <ITEM>τα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>644 книги</ITEM>
 <ITEM>τα εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>1.644 книги</ITEM>
 <ITEM>τα χίλια εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>533.644 книги</ITEM>
 <ITEM>οι πεντακόσιες τριάντα τρεις χιλιάδες εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
<ROW>
 <ITEM>964.533.644 книги</ITEM>
 <ITEM>τα εννιακόσια εξήντα τέσσερα εκατομμύρια πεντακόσιες τριάντα τρεις χιλιάδες εξακόσια σαράντα τέσσερα βιβλία</ITEM>
</ROW>
</TABULAR>

</PARA>
</SECTION>

<SECTION>
<HEADER>Арифметические действия</HEADER>
<PARA>
При счёте и в математике используются формы числительных среднего рода.
</PARA>

<PARA>Арифметические действия передаются следующими выражениями:</PARA>

<TABULAR NOFRAME="1">
<ROW>
 <ITEM>2 + 2 = 4</ITEM>
 <ITEM>δύο <DICTREF>συν</DICTREF> δύο <DICTREF>ίσον</DICTREF> (или κάνουν) τέσσερα</ITEM>
</ROW>

<ROW>
 <ITEM>5 - 3 = 2</ITEM>
 <ITEM>πέντε <DICTREF>μείον</DICTREF> (или πλην) τρία ίσον δύο</ITEM>
</ROW>

<ROW>
 <ITEM>7 x 4 = 28</ITEM>
 <ITEM>επτά <DICTREF>επί</DICTREF> τέσσερα δίνουν είκοσι οκτώ или
       επτά οι τέσσερες δίνουν είκοσι οκτώ</ITEM>
</ROW>

<ROW> 
 <ITEM>10 : 2 = 5</ITEM>
 <ITEM>δέκα <DICTREF>διά</DICTREF> δύο ίσον πέντε</ITEM>
</ROW>
</TABULAR>

<ANCHOR ID="toisekato" />
<PARA>
Процентные выражения читаются так: 15% - <EXAMPLE>δέκα τοις
εκατό</EXAMPLE>. (<EXAMPLE>τοις</EXAMPLE> - дательный падеж
<XREF REF="k_arthra">определённого артикля</XREF> кафаревусы.)
</PARA>

</SECTION>

</PAGE>

<PAGE HEADER="Порядковые числительные">

<PARA>
Порядковые числительные склоняются как
<XREF REF="epitheta:os:h:o">прилагательные на
<FLECT SPLIT=",">ος, η, ο</FLECT></XREF>. В следующей таблице приведены формы
мужского рода порядковых числительных:
</PARA>

<ANCHOR ID="taktika" />
<TABULAR ALTERNATE="1" COLHEADING="std">
<ROW> <ITEM></ITEM>    <ITEM>Количественное</ITEM> <ITEM>Порядковое</ITEM> </ROW>
<SEPARATOR>Единицы</SEPARATOR>
<ROW> <ITEM>1</ITEM>   <ITEM> ένα</ITEM>         <ITEM>πρώτος</ITEM></ROW>
<ROW> <ITEM>2</ITEM>   <ITEM> δύο</ITEM>         <ITEM>δεύτερος</ITEM></ROW>
<ROW> <ITEM>3</ITEM>   <ITEM> τρία</ITEM>        <ITEM>τρίτος </ITEM></ROW>
<ROW> <ITEM>4</ITEM>   <ITEM> τέσσερα</ITEM>     <ITEM>τέταρτος </ITEM></ROW>
<ROW> <ITEM>5</ITEM>   <ITEM> πέντε</ITEM>       <ITEM>πέμπτος </ITEM></ROW>
<ROW> <ITEM>6</ITEM>   <ITEM> έξι</ITEM>         <ITEM>έκτος </ITEM></ROW>
<ROW> <ITEM>7</ITEM>   <ITEM> επτά/εφτά</ITEM>   <ITEM>έβδομος </ITEM></ROW>
<ROW> <ITEM>8</ITEM>   <ITEM> οκτώ/οχτώ</ITEM>   <ITEM>όγδοος </ITEM></ROW>
<ROW> <ITEM>9</ITEM>   <ITEM> εννέα/εννιά</ITEM> <ITEM>ένατος </ITEM></ROW>
<SEPARATOR>Первый десяток</SEPARATOR>
<ROW> <ITEM>10</ITEM>  <ITEM> δέκα</ITEM>        <ITEM>δέκατος </ITEM></ROW>
<ROW> <ITEM>11</ITEM>  <ITEM> ένδεκα</ITEM>      <ITEM>ενδέκατος </ITEM></ROW>
<ROW> <ITEM>12</ITEM>  <ITEM> δώδεκα</ITEM>      <ITEM>δωδέκατος </ITEM></ROW>
<ROW> <ITEM>13</ITEM>  <ITEM> δεκατρία</ITEM>    <ITEM>δέκατος τρίτος </ITEM></ROW>
<ROW> <ITEM>14</ITEM>  <ITEM> δεκατέσσερα</ITEM> <ITEM>δέκατος τέταρτος </ITEM></ROW>
<SEPARATOR>Десятки</SEPARATOR>
<ROW> <ITEM>20</ITEM>  <ITEM> είκοσι</ITEM>      <ITEM>εικοστός </ITEM></ROW>
<ROW> <ITEM>30</ITEM>  <ITEM> τριάντα</ITEM>     <ITEM>τριακοστός</ITEM></ROW>
<ROW> <ITEM>40</ITEM>  <ITEM> σαράντα</ITEM>     <ITEM>τετρακοστός, σαρακοστός</ITEM></ROW>
<ROW> <ITEM>50</ITEM>  <ITEM> πενήντα</ITEM>     <ITEM>πεντηκοστός</ITEM></ROW>
<ROW> <ITEM>60</ITEM>  <ITEM> εξήντα</ITEM>      <ITEM>εξηκοστός </ITEM></ROW>
<ROW> <ITEM>70</ITEM>  <ITEM> εβδομήντα</ITEM>   <ITEM>εβδομηκοστός </ITEM></ROW>
<ROW> <ITEM>80</ITEM>  <ITEM> ογδόντα</ITEM>     <ITEM>ογδοηκοστός</ITEM></ROW>
<ROW> <ITEM>90</ITEM>  <ITEM> ενενήντα</ITEM>    <ITEM>εννενηκοστός </ITEM></ROW>
	     
<SEPARATOR>Сотни</SEPARATOR>
<ROW><ITEM>100</ITEM><ITEM>  εκατό</ITEM>        <ITEM>εκατοστός</ITEM></ROW>
</TABULAR>
</PAGE>

<PAGE HEADER="Числительные наречия">
<PARA>
Числительные наречия обозначают количество раз, которое было совершено
действие (ср. рус. <IT>однажды</IT>, <IT>дважды</IT>). В греческом языке
числительные наречия образуются, как правило, описательно, с
помощью существительного <DICTREF>φορά</DICTREF>:
<EXAMPLE>μια φορά</EXAMPLE> <TRANS>однажды</TRANS>,
<EXAMPLE>δύο φορές</EXAMPLE> дважды, и тп.
</PARA>

<KATHAREVUSA>
<PARA>
В кафаревусе существуют синтетические формы, образованные с помощью
суффикса <FLECT>άκις</FLECT>.
</PARA>

<TABULAR ALTERNATE="1" COLHEADING="std">
<ROW>
 <ITEM>Наречие</ITEM>
 <ITEM>Значение</ITEM>
</ROW>
<SEPARATOR>1-10</SEPARATOR>
<ROW>
 <ITEM></ITEM>
 <ITEM>однажды</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>дважды</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>трижды</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>четырежды</ITEM>
</ROW>
<ROW>
 <ITEM>πεντάκις</ITEM>
 <ITEM>пять раз</ITEM>
</ROW>
<ROW>
 <ITEM>εξάκις</ITEM>
 <ITEM>шесть раз</ITEM>
</ROW>
<ROW>
 <ITEM>επτάκις</ITEM>
 <ITEM>семь раз</ITEM>
</ROW>
<ROW>
 <ITEM>οκτάκις</ITEM>
 <ITEM>восемь раз</ITEM>
</ROW>
<ROW>
 <ITEM>εννεάκις</ITEM>
 <ITEM>девять раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκάκις</ITEM>
 <ITEM>десять раз</ITEM>
</ROW>
<SEPARATOR>11-19</SEPARATOR>
<ROW>
 <ITEM>ενδεκάκις</ITEM>
 <ITEM>одиннадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δωδεκάκις</ITEM>
 <ITEM>двенадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκατριάκις</ITEM>
 <ITEM>тринадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκατετράκις</ITEM>
 <ITEM>четырнадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαπεντάκις</ITEM>
 <ITEM>пятнадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαεξάκις</ITEM>
 <ITEM>шестнадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαεπτάκις</ITEM>
 <ITEM>семнадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαοκτάκις</ITEM>
 <ITEM>восемнадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>δεκαεννεάκις</ITEM>
 <ITEM>девятнадцать раз</ITEM>
</ROW>
<SEPARATOR>20-90</SEPARATOR>
<ROW>
 <ITEM>εικοσάκις</ITEM>
 <ITEM>двадцать раз</ITEM>
</ROW>
<ROW>
 <ITEM>τριακοντάκις</ITEM>
 <ITEM>тридцать раз</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>сорок раз</ITEM>
</ROW>
<ROW>
 <ITEM>πεντηκοντάκις</ITEM>
 <ITEM>пятьдесят раз</ITEM>
</ROW>
<ROW>
 <ITEM>εξηκοντάκις</ITEM>
 <ITEM>шестьдесят раз</ITEM>
</ROW>
<ROW>
 <ITEM>εβδομηκονδάκις</ITEM>
 <ITEM>семдесят раз</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>восемдесят раз</ITEM>
</ROW>
<ROW>
 <ITEM>ενενηκοντάκις</ITEM>
 <ITEM>девяносто раз</ITEM>
</ROW>

<SEPARATOR>100-900</SEPARATOR>
<ROW>
 <ITEM>εκατοκονδάκις</ITEM>
 <ITEM>сто раз</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>двести раз</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>триста раз</ITEM>
</ROW>
<ROW>
 <ITEM></ITEM>
 <ITEM>четыреста раз</ITEM>
</ROW>
<ROW>
 <ITEM>πεντακοσιάκις</ITEM>
 <ITEM>пятьсот раз</ITEM>
</ROW>
<ROW>
 <ITEM>εξακοσιάκις</ITEM>
 <ITEM>шестьсот раз</ITEM>
</ROW>
<ROW>
 <ITEM>επτακοσιάκις</ITEM>
 <ITEM>семьсот раз</ITEM>
</ROW>
<ROW>
 <ITEM>οκτακοσιάκις</ITEM>
 <ITEM>восемьсот раз</ITEM>
</ROW>
<ROW>
 <ITEM>εννεακοσιάκις</ITEM>
 <ITEM>девятьсот раз</ITEM>
</ROW>
</TABULAR>
</KATHAREVUSA>

</PAGE>

<PAGE>
 <HEADER>Дроби</HEADER>
<PARA>
Дроби образуются по следующему правилу. Числитель (ο αριθμητής)
выражается <XREF REF="apolyta">количественным числительным</XREF>
среднего рода, знаменатель (ο παρονομαστής) --
<XREF REF="taktika">порядковым числительным</XREF> среднего рода (при котором
подразумевается слово <DICTREF>μέρος</DICTREF>):

<TABULAR BESTFIT="1" NOFRAME="1">
<ROW>
<ITEM>1/4</ITEM>
<ITEM>ένα τέταρτο</ITEM>
</ROW>

<ROW>
<ITEM>3/4</ITEM>
<ITEM>τρία τέταρτα</ITEM>
</ROW>

<ROW>
<ITEM>7/8</ITEM>
<ITEM>επτά όγδοα</ITEM>
</ROW>

<ROW>
<ITEM>1/100</ITEM>
<ITEM>ένα εκατοστό</ITEM>
</ROW>

<ROW>
<ITEM>1/1000</ITEM>
<ITEM>ένα χιλιοστό</ITEM>
</ROW>

</TABULAR>

</PARA> 
</PAGE>

</CHAPTER>

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: cyrillic-jcuken -->
<!-- alternative-dictionary: "russian" -->
<!-- End: -->
