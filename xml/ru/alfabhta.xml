<!--

  Copyright (C) 2004 Sergey Poznyakoff

  Permission is granted to copy, distribute and/or modify this document
  under the terms of the GNU Free Documentation License, Version 1.2
  or any later version published by the Free Software Foundation;
  with no Invariant Sections, no Front-Cover Texts, and no Back-Cover
  Texts.  A copy of the license is included in the file COPYING.FDL  -->

<PAGE PREFIX="alfabhta">
 <HEADER>Письменность</HEADER>

<SECTION>
 <HEADER>Алфавит</HEADER>

<PARA> 
Греческий алфавит описан в следующей таблице. Произношение каждого
знака приводится в <DFN>международной фонетической транскрипции
(IPA)</DFN>. Там где это возможно, приводится также русское
соответствие. Подробно произношение звуков и их сочетаний описано в
следующем разделе.
</PARA>
 
<TABULAR COLHEADING="std">
<ROW>
 <ITEM>Знак</ITEM>
 <ITEM>Название</ITEM>
 <ITEM>IPA</ITEM>
 <ITEM>Русс.</ITEM>

 <ITEM>Знак</ITEM>
 <ITEM>Название</ITEM>
 <ITEM>IPA</ITEM>
 <ITEM>Русс.</ITEM>
</ROW>
<ROW>
 <ITEM>α Α</ITEM>
 <ITEM>άλφα  </ITEM>
 <ITEM>ɑ</ITEM>
 <ITEM>а</ITEM>

 <ITEM>ν  Ν </ITEM>
 <ITEM>νυ</ITEM>
 <ITEM>n </ITEM>
 <ITEM>н </ITEM>
</ROW>
<ROW>
 <ITEM>β Β</ITEM>
 <ITEM>βήτα</ITEM>
 <ITEM>v</ITEM>
 <ITEM>в</ITEM>

 <ITEM>ξ  Ξ </ITEM>
 <ITEM>ξι</ITEM>
 <ITEM>ks</ITEM>
 <ITEM>кс</ITEM>
</ROW>
<ROW>
 <ITEM>γ Γ</ITEM>
 <ITEM>γάμμα</ITEM>
 <ITEM>ɤ</ITEM>
 <ITEM><XREF REF="gamma"><SUPERSCRIPT>(I)</SUPERSCRIPT></XREF></ITEM>

 <ITEM>ο  Ο </ITEM>
 <ITEM>όμικρον</ITEM>
 <ITEM>o </ITEM>
 <ITEM>о </ITEM>
</ROW>
<ROW>
 <ITEM>δ Δ</ITEM>
 <ITEM>δέλτα </ITEM>
 <ITEM>ð</ITEM>
 <ITEM><XREF REF="delta"><SUPERSCRIPT>(II)</SUPERSCRIPT></XREF></ITEM>

 <ITEM>π  Π </ITEM>
 <ITEM>πι   </ITEM>
 <ITEM>p </ITEM>
 <ITEM>п </ITEM>
 
</ROW>
<ROW>
 <ITEM>ε Ε</ITEM>
 <ITEM>έψιλον</ITEM>
 <ITEM>ɛ</ITEM>
 <ITEM>е</ITEM>

 <ITEM>ρ  Ρ </ITEM>
 <ITEM>ρω</ITEM>
 <ITEM>r </ITEM>
 <ITEM>р </ITEM>

</ROW>
<ROW>
 <ITEM>ζ Ζ</ITEM>
 <ITEM>ζήτα</ITEM>
 <ITEM>z</ITEM>
 <ITEM>з</ITEM>

 <ITEM>σ,ς<FOOTNOTE>Форма <SAMP>σ</SAMP> используется в начале и
середине слова, <SAMP>ς</SAMP> -- в конце слова.</FOOTNOTE> Σ</ITEM>
 <ITEM>σιγμα</ITEM>
 <ITEM>s</ITEM>
 <ITEM>с <XREF REF="sigma"><SUPERSCRIPT>(V)</SUPERSCRIPT></XREF></ITEM>
</ROW>
<ROW>
 <ITEM>η Η</ITEM>
 <ITEM>ήτα</ITEM>
 <ITEM>i</ITEM>
 <ITEM>и</ITEM>

 <ITEM>τ   Τ</ITEM>
 <ITEM>ταυ</ITEM>
 <ITEM>t </ITEM>
 <ITEM>т </ITEM>
 
</ROW>
<ROW>
 <ITEM>θ Θ</ITEM>
 <ITEM>θήτα</ITEM>
 <ITEM>θ</ITEM>
 <ITEM><XREF REF="thita"><SUPERSCRIPT>(III)</SUPERSCRIPT></XREF> </ITEM>

 <ITEM>υ  Υ </ITEM>
 <ITEM>ύψιλον </ITEM>
 <ITEM>i </ITEM>
 <ITEM>и </ITEM>
 
</ROW>
<ROW>
 <ITEM>ι Ι</ITEM>
 <ITEM>ιώτα</ITEM>
 <ITEM>i</ITEM>
 <ITEM>и</ITEM>

 <ITEM>φ  Φ </ITEM>
 <ITEM>φι   </ITEM>
 <ITEM>f </ITEM>
 <ITEM>ф </ITEM>
 
</ROW>
<ROW>
 <ITEM>κ Κ</ITEM>
 <ITEM>κάππα</ITEM>
 <ITEM>k</ITEM>
 <ITEM>к</ITEM>

 <ITEM>χ  Χ </ITEM>
 <ITEM>χι   </ITEM>
 <ITEM>x </ITEM>
 <ITEM>х </ITEM>
 
</ROW>
<ROW>
 <ITEM>λ Λ</ITEM>
 <ITEM>λάμδα</ITEM>
 <ITEM>l</ITEM>
 <ITEM><XREF REF="lambda"><SUPERSCRIPT>(IV)</SUPERSCRIPT></XREF></ITEM>

 <ITEM>ψ Ψ</ITEM>
 <ITEM>ψι   </ITEM>
 <ITEM>ps</ITEM>
 <ITEM>пс</ITEM>
 
</ROW>
<ROW>
 <ITEM>μ Μ</ITEM>
 <ITEM>μυ</ITEM>
 <ITEM>m</ITEM>
 <ITEM>м</ITEM>

 <ITEM>ω Ω</ITEM>
 <ITEM>ωμέγα</ITEM>
 <ITEM>o </ITEM>
 <ITEM>о </ITEM>
 
</ROW>
</TABULAR>
</SECTION>

<SECTION>
 <HEADER>Диакритические знаки</HEADER>

<PARA>В новогреческом языке ударение обозначается в каждом слове,
содержащем более одного слога: <SAMP>διαβάζω</SAMP>,
<SAMP>μπορώ</SAMP>.</PARA>

<PARA>Дополнительно, некоторые односложные слова также отмечаются
знаком ударения, если это необходимо, чтобы отличить их от омонимов:
<SAMP>η</SAMP> - определённый артикль женского рода, <SAMP>ή</SAMP> -
<TRANS>или</TRANS>; <SAMP>πού</SAMP> - <TRANS>где</TRANS>,
<SAMP>που</SAMP> - <TRANS>который</TRANS>.</PARA> 

<PARA>Если ударным слогом является дифтонг, то знак ударения ставится
над его вторым компонентом: <EXAMPLE>αύρα</EXAMPLE>,
<EXAMPLE>αίμα</EXAMPLE>.
</PARA>

<PARA>Если два гласных, обычно образующие <XREF REF="diphtong">дифтонг</XREF>,
должны читаться раздельно, над вторым из них ставится знак διαίρεσις -
две точки над буквой: <SAMP>βοϊδολάτης</SAMP>. В современной
орфографии диерезис не ставится, если первый из гласных несёт на себе
ударение: <SAMP>βόισμα</SAMP>. Если диерезис и ударение стоят над
одной гласной, то ударение ставится над диерезисом: <SAMP>βοΐζω</SAMP>.</PARA>

</SECTION>

</PAGE> 

<!-- Local Variables: -->
<!-- mode: ellinika -->
<!-- buffer-file-coding-system: utf-8 -->
<!-- alternative-input-method: cyrillic-jcuken -->
<!-- alternative-dictionary: "russian" -->
<!-- End: -->
