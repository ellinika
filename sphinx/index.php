<?php
/* Web search for Ellinika
   Copyright (C) 2014 Sergey Poznyakoff
  
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

require ("sphinxapi.php");

function read_config($file) {
	global $config;
	
	$fp = fopen($file, "r");
	if (!$fp)
		die("can't open config file $file");
	while (!feof($fp)) {
		$line = fgets($fp);
		$line = preg_replace('/(^\s+)|(\s+$)|(#.*)/', '', $line);
		if ($line == '') continue;
		if (preg_match('/(.+?)\s*=\s*(.*)/', $line, $m)) {
			$config[$m[1]] = $m[2];
		}
	}
	fclose($fp);
}

$cfg = getenv('ELLINIKA_INDEX_CONFIG');
if (isset($cfg))
	read_config($cfg);
else
	die("configuration file not set");

$base_url = $config['base-url'];
$base_dir = $config['base-directory'];
$index = $config['index'];

if (isset($config['id-directory'])) {
	$iddir = $config['id-directory'];
} else {
	$iddir = "$base_dir/sphinx/var";
}

if ($_REQUEST['l']) {
	$index = preg_replace(array('/\$l(?=[^\w])/',
				    '/\${l}/'),
			      array($_REQUEST['l'],
				    $_REQUEST['l']), $index);
	$htmldir = $base_dir.'/'.$_REQUEST['l'];
} else {
	header("Location: $base_url");
	exit(0);
}

function print_page_title($id, $cl, $q) {
	global $iddir;
	global $index;

	$dom = new DOMDocument;
	if (!$dom->loadHTMLFile("$iddir/$id")) {
		return "<div class=\"err\">Can't load file $iddir/$id</div>\n";
	}
	$items = $dom->getElementsByTagName('title');
	$r = "";
	if ($items->length) {
		$href = readlink("$iddir/$id");
		if (preg_match('/.*\/(.+?\/.+?\.html)/', $href, $matches)) {
			$href = "/".$matches[1];
		} 
		$r .= "<a href=\"$href\">".$items->item(0)->nodeValue."</a>\n";
	}

	$xpath = new DOMXPath($dom);
	$results = $xpath->query("//div[@class='content-inner']");
	$a = array();
	if ($results) {
		for ($i = 0; $i < $results->length; $i++) {
			$a[] = $results->item($i)->nodeValue;
		}
	}
	
	$r .= '<p>';
	foreach ($cl->BuildExcerpts($a, $index, $q,
				    array('limit' => 256)) as
		 $line) {
		$r .= "$line";
	}
	$r .= '</p>';
	return $r;
}

function dosearch($q) {
	global $index;
	$ret = "";
	
	$ret .= "<h2>'".$q."'</h2>";
	$cl = new SphinxClient();
	#$cl->SetServer ( $host, $port );
	$cl->SetArrayResult(false);
	$cl->SetFieldWeights(array('title' => 5,
		                   'content' => 1));
	$cl->SetSortMode(SPH_SORT_RELEVANCE);
	$result = $cl->Query($q, $index);
//	print "<!-- \n";
//	print_r($result);
//	print "-->\n";
	if ($result === false) {
	        $ret .= "<div class=\"err\">Query failed: " . $cl->GetLastError() . ".</div>\n";
	} else {
		$ret .= "<div class=\"res\">\n";
		$ret .= "<ol class=\"res\">\n";
		foreach ($result['matches'] as $id => $val) {
			$ret .= "<li>";
			$ret .= print_page_title($id, $cl, $q);
			$ret .= "</li>";
		}
		$ret .= "</ol>\n";
		$ret .= "</div>\n";
	}
	return $ret;
}

	
$fp = fopen("$htmldir/search.html", "r");
if (!$fp) die();

while (!feof($fp)) {
    $line = preg_replace_callback('/(@@result@@|@@args@@)/',
		function ($matches) {
                    if ($matches[1] == '@@result@@') {
  	                 if ($_REQUEST['q']) {
			     return dosearch($_REQUEST['q']);
                         } else {
	                     return '<div class="err">'.
				    'No query supplied'.
				    '</div>';
                         }
                    } else if ($matches[1] == '@@args@@') {
  	                 if ($_REQUEST['q']) {
 	                     return "&q=".$_REQUEST['q'];
                         }
                    }
                }, fgets($fp)); 
    print $line;
}
fclose($fp);

?>
